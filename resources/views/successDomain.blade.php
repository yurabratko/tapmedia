<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'TapMedia') }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
</head>
<body>
    <div class="container">
        <br>
        <div class="alert alert-success">
            <div class="container">
                <b>Bad domain added!</b>
            </div>
        </div>
    </div>
</body>
</html>
