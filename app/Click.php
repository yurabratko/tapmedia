<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\BadDomains;

/**
 * Class Click
 * @package App
 */
class Click extends Model
{
    /**
     * @return array
     * @throws \Exception
     */
    public static function getAllClicks()
    {
        try {
            return DB::select('SELECT * FROM `table_click`');

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with selecting click data', 400);
        }
    }

    /**
     * @param Request $request
     * @param string $param1
     * @param string $param2
     * @return array
     * @throws \Exception
     */
    public static function setNewClick(Request $request, string $param1, string $param2): array
    {
        if ($click_status = self::checkForUnique(self::getReferrerUri(), $param1)) {

            if (BadDomains::checkIfBadDomain(self::getReferrerUri())) {
                $click_status = self::setBadDomainCount($click_status['click_id']);
            }

            return $click_status;
        }

        try {
            DB::insert('INSERT INTO `table_click` (click_id, user_agent, ip, ref, param1, param2) VALUES (?, ?, ?, ?, ?, ?)',
                array($click_id = self::getUuid(),
                    $request->server('HTTP_USER_AGENT'),
                    $request->ip(),
                    self::getReferrerUri(),
                    $param1,
                    $param2));

            return ['new_click' => true,
                'bad_domain' => false,
                'click_id' => $click_id];

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with inserting new click', 400);
        }
    }

    /**
     * @param string $ref
     * @param string $param1
     * @return array|bool
     * @throws \Exception
     */
    private static function checkForUnique(string $ref, string $param1)
    {
        try {
            $results = DB::select('SELECT * FROM `table_click` WHERE `ref` = ? AND `param1` = ?', array($ref, $param1));

            if ($results) {
                foreach ($results as $result) {
                    DB::update('UPDATE `table_click` SET `error` = `error` + 1 WHERE `click_id` = ?', array($result->click_id));

                    return ['new_click' => false,
                        'bad_domain' => false,
                        'click_id' => $result->click_id];
                }

            } else {
                return false;
            }

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with selecting data', 400);
        }
    }

    /**
     * @return Uuid
     * @throws \Exception
     */
    private static function getUuid(): Uuid
    {
        return Uuid::generate();
    }

    /**
     * @return string
     */
    private static function getReferrerUri(): string
    {
        return isset($_SERVER["HTTP_REFERER"]) ? $ref = $_SERVER["HTTP_REFERER"] : $ref = '';
    }

    /**
     * @param string $click_id
     * @return array
     * @throws \Exception
     */
    private static function setBadDomainCount(string $click_id): array
    {
        try {
            DB::update('UPDATE `table_click` SET `bad_domain` = `bad_domain` + 1 WHERE `click_id` = ?', array($click_id));

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with updating bad domain count', 400);
        }

        return ['new_click' => false,
            'bad_domain' => true,
            'click_id' => $click_id];
    }
}
