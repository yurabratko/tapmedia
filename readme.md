## Installing TapMedia App

Clone this app into main directory and install docker with docker-compose.
To build the image run the command below in the root of the Docker project directory:
```
docker-compose up
```
Go to your browser and visit the URL http://localhost:8000 and you should see the main page with clicks.

### Application options:
When you added some clicks, all info been in:
```
http://localhost:8000
```
For add new click use(params must be only 2):
```
http://localhost:8000/click/param1/ANY_PARAM/param2/ANY_PARAM
```
For saw all bad domains, you can go:
```
http://localhost:8000/bad-domains
```
For add new bad domains(Example domains: google.com, localhost, ukr.net):
```
http://localhost:8000/bad-domain/name/google.com
```
