<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class BadDomains
 * @package App
 */
class BadDomains extends Model
{

    public const HTTP = 'http://';
    public const HTTPS = 'https://';

    /**
     * @return array
     * @throws \Exception
     */
    public static function getAllBadDomains()
    {
        try {
            return DB::select('SELECT * FROM `table_bad_domains`');

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with selecting bad domain names', 400);
        }
    }

    /**
     * @param $name
     * @return bool
     * @throws \Exception
     */
    public static function setBadDomain($name)
    {
        try {
            if (self::checkIfBadDomain($name)) {
                throw new \Exception('We already have this domain!', 400);
            }

            DB::insert('INSERT INTO `table_bad_domains` (name) VALUES (?)', array($name));
            return true;

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with inserting bad domain names', 400);
        }
    }

    /**
     * @param string $ref
     * @return bool
     * @throws \Exception
     */
    public static function checkIfBadDomain(string $ref): bool
    {
        try {
            $clean_ref = str_replace([self::HTTP, self::HTTPS, '/'], '', $ref);

            $results = DB::select('SELECT 1 FROM `table_bad_domains` WHERE `name` IN (?, ?)', array($ref, $clean_ref));

            if ($results) {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $exception) {
            throw new \Exception('Something wrong with find bad domains', 400);
        }
    }
}
