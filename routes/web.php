<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ClickController;
use App\Http\Controllers\BadDomainController;

Route::get('/', function () {

    $click = new ClickController();
    $clicks = $click->get();

    return view('clicks', ['clicks' => $clicks]);
});
Route::get('/bad-domains', function () {

    $badDomain = new BadDomainController();
    $badDomains = $badDomain->get();

    return view('badDomains', ['bad_domains' => $badDomains]);
});
Route::get('click/param1/{param1?}/param2/{param2?}', 'ClickController@set');
Route::get('click/param1/{param1?}', 'ClickController@set');
Route::get('click/param2/{param2?}', 'ClickController@set');
Route::get('success/{click_id}', function ($click_id) {
    return view('success', ['click_id' => $click_id]);
})->name('success');
Route::get('success-add-bad-domain', function () {
    return view('successDomain');
})->name('success_domain');
Route::get('error/{click_id}', function () {
    return view('error', ['error_add_click' => true]);
})->name('error');
Route::get('error-bad-domain/{click_id}', function () {
    return view('error', ['error_bad_domain' => true]);
})->name('error-bad-domain');
Route::get('bad-domain/name/{ref?}', 'BadDomainController@set');
