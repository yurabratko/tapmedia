<?php

namespace App\Interfaces;

/**
 * Interface IBadDomainController
 * @package App\Interfaces
 */
interface IBadDomainController
{
    /**
     * Getter for for all names in bad domain table
     *
     * @return mixed
     */
    public function get();

    /**
     * Setter for new names in bad domain table
     *
     * @param string|null $ref
     * @return mixed
     */
    public function set(string $ref = null);
}
