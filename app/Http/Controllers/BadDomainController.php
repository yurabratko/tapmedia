<?php

namespace App\Http\Controllers;

use App\Interfaces\IBadDomainController;
use App\BadDomains;

/**
 * Class BadDomainController
 * @package App\Http\Controllers
 */
class BadDomainController extends Controller implements IBadDomainController
{
    /**
     * @return array
     * @throws \Exception
     */
    public function get()
    {
        return BadDomains::getAllBadDomains();
    }

    /**
     * @param string|null $ref
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function set(string $ref = null)
    {
        if (is_null($ref)) {
            throw new \Exception('Referrer name required', 400);
        }

        if (BadDomains::setBadDomain($ref)) {
            return redirect()->route('success_domain');
        }
    }
}
