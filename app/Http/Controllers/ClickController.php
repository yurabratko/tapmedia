<?php

namespace App\Http\Controllers;

use App\Interfaces\IClickController;
use Illuminate\Http\Request;
use App\Click;

/**
 * Class ClickController
 * @package App\Http\Controllers
 */
class ClickController extends Controller implements IClickController
{
    /**
     * @return array
     * @throws \Exception
     */
    public function get()
    {
        return Click::getAllClicks();
    }

    /**
     * @param Request $request
     * @param string $param1
     * @param string $param2
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function set(Request $request, string $param1 = null, string $param2 = null)
    {
        if (is_null($param1) || is_null($param2)) {
            throw new \Exception('There must be two params for added click!', 400);
        }

        $click_status = Click::setNewClick($request, $param1, $param2);

        if ($click_status['new_click']) {
            return redirect()->route('success', ['click_id' => $click_status['click_id']]);
        } elseif ($click_status['bad_domain']) {
            return redirect()->route('error-bad-domain', ['click_id' => $click_status['click_id']]);
        } else {
            return redirect()->route('error', ['click_id' => $click_status['click_id']]);
        }
    }
}
