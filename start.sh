#!/bin/bash

cd /var/www

if [[ ! -d "/var/www/vendor" ]]; then
    ./composer.phar update
    ./composer.phar dump-autoload -o
fi

cp -f php/example_settings .env
php artisan migrate --force
php-fpm -F -R

exec php
