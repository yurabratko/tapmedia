<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClick extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_click', function (Blueprint $table) {
            $table->uuid('click_id');
            $table->string('user_agent');
            $table->string('ip', 20);
            $table->string('ref');
            $table->string('param1');
            $table->string('param2');
            $table->integer('error')->default(0);
            $table->integer('bad_domain')->default(0);

            $table->primary('click_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_click');
    }
}
