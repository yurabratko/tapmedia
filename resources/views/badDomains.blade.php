@extends('layouts.app')

@section('content')
    @if(isset($bad_domains) && !empty($bad_domains))
        <div class="container-fluid">
            <br>
            <table id="clicks" class="ui celled table" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($bad_domains as $bad_domain)
                    <tr>
                        <td>{{ $bad_domain->id }}</td>
                        <td>{{ $bad_domain->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="container">
            <br>
            <div class="alert alert-info">
                <div class="container">
                    <b>No results!</b>
                </div>
            </div>
        </div>
    @endif
@endsection
