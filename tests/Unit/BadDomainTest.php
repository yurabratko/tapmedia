<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Controllers\BadDomainController;

/**
 * Class BadDomainTest
 * @package Tests\Unit
 */
class BadDomainTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testBadDomainController(){
        $badDomain = new BadDomainController();
        $this->assertIsArray($badDomain->get(), 'Wrong type from bad domain getter');
    }
}
