<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Controllers\ClickController;

/**
 * Class ClickTest
 * @package Tests\Unit
 */
class ClickTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testClickController(){
        $click = new ClickController();
        $this->assertIsArray($click->get(), 'Wrong type from click getter');
    }
}
