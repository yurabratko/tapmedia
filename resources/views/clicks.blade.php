@extends('layouts.app')

@section('content')
    @if(isset($clicks) && !empty($clicks))
        <div class="container-fluid">
            <br>
            <table id="clicks" class="ui celled table" style="width:100%">
                <thead>
                <tr>
                    <th>Click Id</th>
                    <th>User agent</th>
                    <th>IP</th>
                    <th>Referrer</th>
                    <th>Param 1</th>
                    <th>Param 2</th>
                    <th>Error</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($clicks as $click)
                    <tr>
                        <td>{{ $click->click_id }}</td>
                        <td>{{ $click->user_agent }}</td>
                        <td>{{ $click->ip }}</td>
                        <td>{{ $click->ref }}</td>
                        <td>{{ $click->param1 }}</td>
                        <td>{{ $click->param2 }}</td>
                        <td>{{ $click->error }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="container">
            <br>
            <div class="alert alert-info">
                <div class="container">
                    <b>No results yet!</b>
                </div>
            </div>
        </div>
    @endif
@endsection
