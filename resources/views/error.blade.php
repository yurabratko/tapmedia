<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'TapMedia') }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
</head>
<body>
    @if(isset($error_add_click))
        <div class="container">
            <br>
            <div class="alert alert-danger">
                <div class="container">
                    <b>That's non-unique click!</b>
                </div>
            </div>
        </div>
    @elseif(isset($error_bad_domain))
        <div class="container">
            <br>
            <div class="alert alert-danger">
                <div class="container">
                    <b>That's bad domain!</b>
                </div>
            </div>
        </div>

        <script>
            setTimeout(function() {
                window.location="https://google.com";
            }, 5000);
        </script>
    @endif
</body>
</html>
