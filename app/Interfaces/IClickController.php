<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

/**
 * Interface IClickController
 * @package App\Interfaces
 */
interface IClickController
{
    /**
     * Getter for for all data in click table
     *
     * @return mixed
     */
    public function get();

    /**
     * Setter for new data in click table
     *
     * @param Request $request
     * @param string|null $param1
     * @param string|null $param2
     * @return mixed
     */
    public function set(Request $request, string $param1 = null, string $param2 = null);
}
